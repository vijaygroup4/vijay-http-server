//importing http and uuid modules
const http = require("http");
const uuid = require("uuid");

//creating the http server
const server = http.createServer((request, response) => {
  //filtering for different  urls and sending different responses respectively
  if (request.method === "GET" && request.url === "/html") {
    response.writeHead(200, { "Content-Type": "text/html" });
    //writing the html content
    response.write(`
    <!DOCTYPE html>
      <html>
        <head>
        </head>
        <body>
            <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
            <p> - Martin Fowler</p>

        </body>
      </html>
    `);
    response.end();
  } else if (request.method === "GET" && request.url === "/json") {
    response.writeHead(200, { "Content-Type": "application/json" });
    let data = {
      slideshow: {
        author: "Yours Truly",
        date: "date of publication",
        slides: [
          {
            title: "Wake up to WonderWidgets!",
            type: "all",
          },
          {
            items: [
              "Why <em>WonderWidgets</em> are great",
              "Who <em>buys</em> WonderWidgets",
            ],
            title: "Overview",
            type: "all",
          },
        ],
        title: "Sample Slide Show",
      },
    };

    //if it is a data,send the data in the form of JSON
    response.write(JSON.stringify(data));
    response.end();
  } else if (request.method === "GET" && request.url === "/uuid") {
    response.writeHead(200, { "Content-Type": "application/json" });
    //generating the random identifier
    let uuidData = {
      uuid: uuid.v4(),
    };
    //converting the uuid data to JSON and sending
    response.write(JSON.stringify(uuidData));
    response.end();
  } else if (request.method === "GET" && request.url.startsWith("/status")) {
    //creating the array of strings of url
    let urlArray = request.url.split("/");
    //converting status code to number
    let statusCode = parseFloat(urlArray[urlArray.length - 1]);
    if (!isNaN(statusCode)) {
      response.writeHead(statusCode, { "Content-Type": "text/plain" });
      response.write(`Response with ${statusCode} code`);
      response.end();
    } else {
      //if it is not a valid status code,then responding with 404
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("Not Found!");
      response.end();
    }
  } else if (request.method === "GET" && request.url.startsWith("/delay/")) {
    //creating the array of strings of url
    let urlArray = request.url.split("/");
    //converting the delay seconds to number
    let delaySeconds = parseFloat(urlArray[urlArray.length - 1]);
    if (!isNaN(delaySeconds)) {
      setTimeout(() => {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.write(`Delayed by ${delaySeconds} seconds`);
        response.end();
      }, delaySeconds * 1000);
    } else {
      //if delay seconds is invalid,then responding with 404
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("Not Found!");
      response.end();
    }
  } else {
    //for other urls,responding with 404
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.write("Not Found!");
    response.end();
  }
});

//making the server to listen on port 3000
server.listen(3000, () => {
  console.log("server running on port 3000");
});
